const express = require('express')
// const morgan = require('morgan')
const swaggerUi = require('swagger-ui-express')
const cors = require('cors')

const swaggerDocument = require('./api/swagger/swagger')
const userRoutes = require('./api/routes/users')
const eventRoutes = require('./api/routes/events')

const app = express()

app.use(cors())
app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use('/uploads', express.static('uploads'))
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))
// app.use(morgan('dev'))

app.use('/users', userRoutes)
app.use('/events', eventRoutes)

const port = process.env.PORT || 8080

app.listen(port, () => {
    console.log(`Server started on port ${port}`)
})