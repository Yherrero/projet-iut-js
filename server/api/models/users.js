const events = require('./events')

const users = [
    {
        _id: 0,
        email: 'jean@mail.com',
        password: '$2a$10$M/p0jLCLrt8Mc96gpmiWcOlpduLmO68/hp7cCsC5Lscb.vH3wHeSG',
        img: 'uploads/1544642889356default.png',
        goingTo: [0, 1, 2, 3]
    },
    {
        _id: 1,
        email: 'morgan@mail.com',
        password: '$2a$10$M/p0jLCLrt8Mc96gpmiWcOlpduLmO68/hp7cCsC5Lscb.vH3wHeSG',
        img: 'uploads/1544642889356default.png',
        goingTo: []
    },
    {
        _id: 2,
        email: 'davide@mail.com',
        password: '$2a$10$M/p0jLCLrt8Mc96gpmiWcOlpduLmO68/hp7cCsC5Lscb.vH3wHeSG',
        img: 'uploads/1544642889356default.png',
        goingTo: [3]
    }
]

// get
function getAll() {
    const res = users.map(user => {
        return {
            _id: user._id,
            email: user.email,
            img: user.img
        }
    })
    return res
}

function findById(userId) {
    return users.find(user => (user._id == userId))
}

function findByEventId(eventId) {
    return users.filter(user => (user.goingTo.includes(parseInt(eventId))))
}

function findByEmail(email) {
    return users.find(user => (user.email == email))
}

function getAllGoingTo(userId) {
    const user = findById(userId)
    if(user) {
        return user.goingTo.map(eventId => events.findById(eventId))
    }
    return []
}

// patch
function editImg(userId, img) {
    const user = findById(userId)
    if(user) {
        user.img = img
    }
}

function registerTo(userId, eventId) {
    const user = findById(userId)
    if(user) {
        user.goingTo.push(eventId)
    }
}

function unregisterTo(userId, eventId) {
    const user = findById(userId)
    if(user) {
        user.goingTo = user.goingTo.filter(el => el != eventId)
    }
}

// post
function addUser(user) {
    users.push(user)
}

// functions
function createId() {
    return Math.max.apply(Math, users.map(user => (user._id + 1)))
}

function exists(email) {
    return users.find(user => (user.email == email)) ? true : false
}

function isValidEmail(email) {
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return !regex.test(email)
}

module.exports = {
    getAll,
    findById,
    findByEmail,
    findByEventId,
    editImg,
    registerTo,
    unregisterTo,
    addUser,
    createId,
    exists,
    isValidEmail,
    getAllGoingTo
}