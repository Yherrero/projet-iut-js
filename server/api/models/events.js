let events = [

    {
        _id: 0,
        creatorId: 2,
        title: 'Party',
        description: 'Come with us in our great party !',
        date: 1546108936137
    },

    {
        _id: 1,
        creatorId: 1,
        title: 'Birthday',
        description: 'Hello everyone, my birthday party comes soon !',
        date: 1545519600000
    },

    {
        _id: 2,
        creatorId: 1,
        title: 'Halloween',
        description: 'Naet comoa lea carin tilea garcie.',
        date: 1545157200000
    },

    {
        _id: 3,
        creatorId: 1,
        title: 'Thanksgiving',
        description: 'lorem ipsum sin dolae',
        date: 1545692400000
    },


]

// get
function getAll() {
    return events
}

function findById(id) {
    return events.find(event => (event._id == id))
}

function findAllByUser(userId) {
    return events.filter(event => (event.creatorId == userId))
}

//patch
function editEvent(id, title, description, date) {
    const event = findById(id)
    event.title = title
    event.description = description
    event.date = date
}

// post
function addEvent(event) {
    events.push(event)
}

// delete
function deleteEvent(id) {
    events = events.filter(event => (event._id != id))
}

// functions 
function createId() {
    return Math.max.apply(Math, events.map(event => (event._id + 1)))
}

module.exports = {
    getAll,
    findAllByUser,
    findById,
    editEvent,
    addEvent,
    createId,
    deleteEvent
}