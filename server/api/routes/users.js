const express = require('express')

const UsersController = require('../controllers/users')
const checkAuth = require('../middleware/check-auth')
const upload = require('../middleware/multer')

const router = express.Router()


/**
 * Create a new user
 */
router.post('/signup', UsersController.signup_user)

/**
 * Log a user in
 */
router.post('/login', UsersController.login_user)

/**
 * Edit picture of a user with his Id
 */
router.patch('/:userId', checkAuth, upload.single('userImg') ,UsersController.edit_user)

/**
 * Register a user to an event
 */
router.patch('/register/:userId/:eventId', checkAuth, UsersController.register_user_to_event)

/**
 * Unregister a user to an event
 */
router.patch('/unregister/:userId/:eventId', checkAuth, UsersController.unregister_user_to_event)

/**
 * Get all users
 */
router.get('/', checkAuth, UsersController.get_users)

/**
 * Get a user by ID
 */
router.get('/:userId', checkAuth, UsersController.get_user_by_id)

/**
 * Get an array of user by eventID
 */
router.get('/events/:eventId', checkAuth, UsersController.get_users_by_event)

/**
 * Get all events registered by one user
 */
router.get('/:userId/events', checkAuth, UsersController.get_events_registered_by)


module.exports = router