const express = require('express')

const EventsController = require('../controllers/events')
const checkAuth = require('../middleware/check-auth')
const upload = require('../middleware/multer')

const router = express.Router()


/**
 * Post a new event
 */
router.post('/', checkAuth, EventsController.create_event)

/**
 * Edit an event
 */
router.patch('/:eventId', checkAuth, EventsController.edit_event)

/**
 * Get all events
 */
router.get('/', EventsController.get_events)

/**
 * Get one event
 */
router.get('/:eventId', checkAuth, EventsController.get_event_by_id)

/**
 * Get all events created by one user
 */
router.get('/createdby/:userId', checkAuth, EventsController.get_events_by_creator)

/**
 * Delete one event
 */
router.delete('/:eventId', checkAuth, EventsController.delete_event)


module.exports = router