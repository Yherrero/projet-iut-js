const config = require('../config')
const users = require('../models/users')
const events = require('../models/events')


/**
 * Post a new event
 * 
 * @param {string} title
 * @param {string} description
 * @param {Date} date
 */
exports.create_event = (req, res) => {
    const description = req.body.description
    const title = req.body.title
    const date = req.body.date
    const creatorId = req.userData.id
    const _id = events.createId()

    // is all data entered ?
    if(!description || !title || !date){
        return res.status(config.errors.missingData.status).json({
            error: config.errors.missingData.message
        })
    }

    // is date valid ?
    // is title and description valid (regex) ?

    events.addEvent({
        _id,
        creatorId,
        title,
        description,
        date
    })
    res.status(200).json({
        message: "New event created",
        id: _id
    })
}

/**
 * Edit an event
 * 
 * @param {string} title
 * @param {string} description
 * @param {Date} date
 */
exports.edit_event = (req, res) => {
    const description = req.body.description
    const title = req.body.title
    const date = req.body.date
    const id = req.params.eventId

    const event = events.findById(id)

    // Is the event exists ?
    if(!event){
        return res.status(config.errors.wrongID.status).json({ 
            message: config.errors.wrongID.message    
        })
    }

    // Has the user the permission to edit ?
    if(event.creatorId != req.userData.id){
        return res.status(config.errors.unauthorized.status).json({ 
            message: config.errors.unauthorized.message    
        })
    }

    // is all data entered ?
    if(!description || !title || !date){
        return res.status(config.errors.missingData.status).json({
            error: config.errors.missingData.message
        })
    }

    // is date valid ?
    // is title and description valid (regex) ?

    events.editEvent(id, title, description, date)
    res.status(200).json({
        message: "Event updated"
    })
}

/**
 * Get all events
 * 
 * @param {Number} max (optional)
 */
exports.get_events = (req, res) => {
    const max = req.query.max
    const result = events.getAll()
    if(max){
        return res.status(200).json({ result: result.slice(0, max) })
        
    }
    res.status(200).json({ result })
}

/**
 * Get one event by id
 * 
 * @param {Number} eventId
 */
exports.get_event_by_id = (req, res) => {
    const event = events.findById(req.params.eventId)
    // No corresponding event
    if(!event){
        return res.status(config.errors.wrongID.status).json({
            message: config.errors.wrongID.message
        })
    }
    res.status(200).json({ event })
}

/**
 * Get all events created by one user
 * 
 * @param {Number} userId
 */
exports.get_events_by_creator = (req, res) => {
    const result = events.findAllByUser(req.params.userId)
    // res is an empty array if event doesn't exists
    res.status(200).json({ result })
}

/**
 * Delete one event
 * 
 * @param {Number} eventId
 */
exports.delete_event = (req, res) => {
    const id = req.params.eventId
    const event = events.findById(id)
    // Is the event exists ?
    if(!event){
        return res.status(config.errors.wrongID.status).json({ 
            message: config.errors.wrongID.message    
        })
    }

    // Has the user the permission to edit ?
    if(event.creatorId != req.userData.id){
        return res.status(config.errors.unauthorized.status).json({ 
            message: config.errors.unauthorized.message    
        })
    }

    events.deleteEvent(id)
    res.status(200).json({
        message: "Event deleted"
    })
}