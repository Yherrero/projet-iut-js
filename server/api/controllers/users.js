const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

const config = require('../config')
const users = require('../models/users')
const events = require('../models/events')


/**
 * Create a new user
 * 
 * @param {string} email
 * @param {string} password
 */
exports.signup_user = (req, res) => {
    const email = req.body.email
    const password = req.body.password

    // is all data entered ?
    if(!email || !password){
        return res.status(config.errors.missingData.status).json({
            message: config.errors.missingData.message
        })
    }

    // is email already taken ?
    if(users.exists(email)){
        return res.status(config.errors.alreadyTakenEmail.status).json({
            message: config.errors.alreadyTakenEmail.message
        })
    }

    // is email valid ?
    if(users.isValidEmail(email)){
        return res.status(config.errors.invalidData.status).json({
            message: config.errors.invalidData.message
        })
    }

    bcrypt.hash(password, 10, (err, hash) => {
        // fail with bcrypt
        if (err) {
            return res.status(500).json({
                message: err
            })
        } else {
            const user = {
                _id: users.createId(),
                email,
                password: hash,
                img: config.defaultUserImage,
                goingTo: []
            }
            const token = jwt.sign(
                {
                    email: user.email,
                    id: user._id
                }, 
                process.env.JWT_KEY, 
                {
                    expiresIn: "1h",
                }
            )
            users.addUser(user)
            res.status(200).json({
                message: "New user created",
                user,
                token
            })
        }
    })
}

/**
 * Log in a user
 * 
 * @param {string} email
 * @param {string} password
 */
exports.login_user = (req, res) => {
    const email = req.body.email
    const password = req.body.password
    const user = users.findByEmail(email)

    // Is email exists ?
    if (!user) {
        return res.status(config.errors.authFailed.status).json({
            message: config.errors.authFailed.message
        })
    }

    bcrypt.compare(password, user.password, (err, result) => {
        // fail with bcrypt
        if (err) {
            return res.status(config.errors.authFailed.status).json({
                message: config.errors.authFailed.message
            })
        }
        if (result) {
            const token = jwt.sign(
                {
                    email: user.email,
                    id: user._id
                }, 
                process.env.JWT_KEY, 
                {
                    expiresIn: "1h",
                }
            )
            return res.status(200).json({
                message: "Auth successful",
                user,
                token
            })
        }
        // Is password corresponding ?
        res.status(config.errors.authFailed.status).json({
            message: config.errors.authFailed.message
        })
    })
}

/**
 * Edit picture of a user with his Id
 * 
 * @param {Number} userId
 */
exports.edit_user = (req, res) => {
    const path = req.file.path.replace(/\\/g,"/")
    users.editImg(req.params.userId, path)
    const userId = req.params.userId

    // Has the user the permission to edit ?
    if(userId != req.userData.id){
        return res.status(config.errors.unauthorized.status).json({ 
            message: config.errors.unauthorized.message    
        })
    }

    // Is the user exists ?
    if(!users.findById(userId)){
        return res.status(config.errors.wrongID.status).json({ 
            message: config.errors.wrongID.message    
        })
    }

    res.status(200).json({ 
        message: "Patch edit user"    
    })
}

/**
 * Register a user to an event
 * 
 * @param {Number} userId
 * @param {Number} eventId
 */
exports.register_user_to_event = (req, res) => {
    const userId = req.params.userId
    const eventId = req.params.eventId
    const user = users.findById(userId)
    const event = events.findById(eventId)
    
    // Has the user the right id ?
    if(userId != req.userData.id){
        return res.status(config.errors.unauthorized.status).json({ 
            message: config.errors.unauthorized.message    
        })
    }
    
    // Is the user the creator ?
    if(userId == event.creatorId){
        return res.status(config.errors.wrongUserID.status).json({ 
            message: config.errors.wrongUserID.message    
        })
    }

    // Is the user exists ?
    if(!user){
        return res.status(config.errors.wrongUserID.status).json({ 
            message: config.errors.wrongUserID.message    
        })
    }

    // Is the event exists ?
    if(!events.findById(eventId)){
        return res.status(config.errors.wrongEventID.status).json({ 
            message: config.errors.wrongEventID.message    
        })
    }

    // Is the event already registered ?
    if(user.goingTo.includes(eventId)) {
        return res.status(config.errors.alreadyRegisteredEvent.status).json({ 
            message: config.errors.alreadyRegisteredEvent.message    
        })
    }

    users.registerTo(userId, eventId)
    res.status(200).json({ 
        message: "Event registered"    
    })
}

/**
 * Unregister a user to an event
 * 
 * @param {Number} userId
 * @param {Number} eventId
 */
exports.unregister_user_to_event = (req, res) => {
    const userId = req.params.userId
    const eventId = req.params.eventId
    const user = users.findById(userId)
    
    // Has the user the right id ?
    if(userId != req.userData.id){
        return res.status(config.errors.unauthorized.status).json({ 
            message: config.errors.unauthorized.message    
        })
    }

    // Is the user exists ?
    if(!user){
        return res.status(config.errors.wrongUserID.status).json({ 
            message: config.errors.wrongUserID.message    
        })
    }

    // Is the event exists ?
    if(!events.findById(eventId)){
        return res.status(config.errors.wrongEventID.status).json({ 
            message: config.errors.wrongEventID.message    
        })
    }

    users.unregisterTo(userId, eventId)
    res.status(200).json({ 
        message: "Event unregistered"    
    })
}

/**
 * Get all users
 */
exports.get_users = (req, res) => {
    const usersList = users.getAll()

    res.status(200).json({ result: usersList })
}

/**
 * Get a user with his Id
 * 
 * @param {Number} userId
 */
exports.get_user_by_id = (req, res) => {
    const user = users.findById(req.params.userId)

    // Is the user exists ?
    if(!user){
        return res.status(config.errors.wrongID.status).json({
            message: config.errors.wrongID.message
        })
    }

    res.status(200).json({ user })
}

/**
 * Get all users that going to an Event
 * 
 * @param {Number} eventId
 */
exports.get_users_by_event = (req, res) => {
    const result = users.findByEventId(req.params.eventId)
    // result is an empty array if event doesn't exists
    res.status(200).json({ result })
}

/**
 * Get all events registered by one user
 * 
 * @param {Number} userId
 */
exports.get_events_registered_by = (req, res) => {
    const events = users.getAllGoingTo(req.params.userId)
    // Wrong user id
    if(!events){
        return res.status(config.errors.wrongUserID.status).json({ 
            message: config.errors.wrongUserID.message    
        })
    }
    res.status(200).json({ events })
}
