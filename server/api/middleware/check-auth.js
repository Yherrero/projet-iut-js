const jwt = require('jsonwebtoken')
const config = require('../config')

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1]
        const decoded = jwt.verify(token, process.env.JWT_KEY, null)
        req.userData = decoded
        next()
    } catch (error) {
        return res.status(config.errors.authFailed.status).json({
            message: config.errors.authFailed.message
        })
    }
}